import os
import torch
import argparse
from transformers import AutoTokenizer, AutoModelForCausalLM, BitsAndBytesConfig, TrainingArguments
from peft import LoraConfig, PeftModel
from datasets import load_dataset_builder, load_dataset
from trl import SFTTrainer
import shutil
import gc
from datasets.utils.logging import disable_progress_bar

GCS_URI_PREFIX = 'gs://'
GCSFUSE_URI_PREFIX = '/gcs/'

def force_gc():
  """Collects garbage immediately to release unused CPU/GPU resources."""
  gc.collect()
  torch.cuda.empty_cache()

def is_gcs_path(input_path: str) -> bool:
  """Checks if the input path is a Google Cloud Storage (GCS) path.

  Args:
      input_path: The input path to be checked.

  Returns:
      True if the input path is a GCS path, False otherwise.
  """
  return input_path is not None and input_path.startswith(
      GCS_URI_PREFIX
  )

def force_gcs_fuse_path(gcs_uri: str) -> str:
  """Converts gs:// uris to their /gcs/ equivalents. No-op for other uris."""
  if is_gcs_path(gcs_uri):
    return (
        GCSFUSE_URI_PREFIX + gcs_uri[len(GCS_URI_PREFIX) :]
    )
  else:
    return gcs_uri

def load_causal_language_model(
    pretrained_model_id: str,
):
    if is_gcs_path(pretrained_model_id):
       temp_model_path = os.path.join('/tmp', 'model')
       print("Begin download pretained model from GCS...\n")
       shutil.copytree(force_gcs_fuse_path(pretrained_model_id), temp_model_path)
       print("End download pretained model from GCS.\n")
       pretrained_model_id = temp_model_path
    compute_dtype = getattr(torch, "float16")
    bnb_config = BitsAndBytesConfig(
        load_in_4bit=True,
        bnb_4bit_quant_type="nf4",
        bnb_4bit_compute_dtype=compute_dtype,
        bnb_4bit_use_double_quant=True,
    )
    model = AutoModelForCausalLM.from_pretrained(
            pretrained_model_id, 
            device_map="auto", 
            quantization_config=bnb_config,
    )
    tokenizer = AutoTokenizer.from_pretrained(
       pretrained_model_id,
       padding_side="right")

    return (model, tokenizer)

def merge_causal_language_model_with_lora(
    pretrained_model_id: str,
    finetuned_lora_model_dir: str,
    merged_model_output_dir: str,
):
    """Merges the base model with the lora adapter."""
    # Loads base model with PRECISION_MODE_16.
    force_gc()
    if is_gcs_path(pretrained_model_id):
       pretrained_model_id = os.path.join('/tmp', 'model')

    model = AutoModelForCausalLM.from_pretrained(
        pretrained_model_id,
        device_map="auto", 
        torch_dtype=torch.float16, 
    )
    tokenizer = AutoTokenizer.from_pretrained(pretrained_model_id)
    # Loads LoRA models.
    model = PeftModel.from_pretrained(model, finetuned_lora_model_dir)

    # Merges base and finetuned LoRA models.
    model = model.merge_and_unload()

    # Saved merged models and tokenizer.
    model.save_pretrained(merged_model_output_dir)
    tokenizer.save_pretrained(merged_model_output_dir)

def main(args):
    MODEL_DIR = force_gcs_fuse_path(args.model_saved_path)
    CHECKPOINT_DIR = force_gcs_fuse_path(args.checkpoint_saved_path)
    TENSORBOARD_DIR = force_gcs_fuse_path(os.environ["AIP_TENSORBOARD_LOG_DIR"])

    os.environ["HF_TOKEN"] = args.hf_token if args.hf_token else ""
    (model, tokenizer) = load_causal_language_model(
        pretrained_model_id=args.model,
        )

    lora_config = LoraConfig(
        lora_alpha=args.lora_alpha,
        lora_dropout=args.lora_dropout,
        r=args.lora_r,
        bias="none",
        task_type="CAUSAL_LM",
        target_modules= ["down_proj","up_proj","gate_proj"]
    )

    disable_progress_bar()
    data_files = {}
    do_eval = False
    if args.validation_dataset:
        print("There is validation dataset, and the do_eval=True.\n")
        data_files = {
            "train": [args.train_dataset],
            "validation": [args.validation_dataset]
        }
        do_eval = True
    else:
        print("There is no validation dataset, and the do_eval=False.\n")
        data_files = {
            "train": [args.train_dataset],
        }
    builder = load_dataset_builder(args.dataset_type, data_files=data_files)
    data = load_dataset(args.dataset_type, data_files=data_files)

    training_arguments = TrainingArguments(
            disable_tqdm=True,
            output_dir=CHECKPOINT_DIR,
            evaluation_strategy=("steps" if do_eval else 'no'),
            optim="paged_adamw_8bit",
            save_steps=args.save_steps,
            save_strategy="steps",
            save_total_limit=3,
            log_level=args.log_level,
            logging_steps=args.logging_steps,
            learning_rate=args.learning_rate,
            eval_steps=args.eval_steps,
            fp16=True,
            do_eval=do_eval,
            per_device_train_batch_size=args.per_device_train_batch_size,
            per_device_eval_batch_size=args.per_device_eval_batch_size,
            gradient_accumulation_steps=args.gradient_accumulation_steps,
            warmup_steps=args.warmup_steps,
            max_steps=args.max_steps,
            lr_scheduler_type="linear",
            report_to=['tensorboard'],
            logging_dir=os.path.join(TENSORBOARD_DIR, "tensorboard"),
    )

    trainer = SFTTrainer(
            model=model,
            train_dataset=data['train'],
            eval_dataset=data['validation'] if do_eval else None,
            peft_config=lora_config,
            dataset_text_field=args.dataset_text_field,
            max_seq_length=args.max_seq_length,
            tokenizer=tokenizer,
            args=training_arguments
    )
    trainer.train()
    trainer = None
    model = None
    tokenizer = None

    output_dir = os.path.join(
          CHECKPOINT_DIR, 'checkpoint-' + str(args.max_steps)
      )
    merge_causal_language_model_with_lora(
        pretrained_model_id=args.model,
        finetuned_lora_model_dir=output_dir,
        merged_model_output_dir=MODEL_DIR,
        )

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process training.')

    parser.add_argument('--hf_token', type=str, help='HF token for the base model')
    parser.add_argument('--model', type=str, help='HF model ID', required=True)
    parser.add_argument('--dataset_type', type=str, help='Dataset file type', default='parquet')
    parser.add_argument('--train_dataset', type=str, help='GCS path for training dataset', required=True)
    parser.add_argument('--validation_dataset', type=str, help='GCS path for validation dataset')
    parser.add_argument('--checkpoint_saved_path', type=str, help='GCS path for checkpoint', default='/worksapce/checkpoint')
    parser.add_argument('--model_saved_path', type=str, help='GCS path for checkpoint', default='/worksapce/model')
    parser.add_argument('--warmup_steps', type=int, default=2)
    parser.add_argument('--max_steps', type=int, default=10)
    parser.add_argument('--save_steps', type=int, default=10)
    parser.add_argument('--lora_alpha', type=int, default=16)
    parser.add_argument('--lora_dropout', type=float, default=0.05)
    parser.add_argument('--lora_r', type=int, default=16)
    parser.add_argument('--log_level', type=str, default='info')
    parser.add_argument('--learning_rate', type=float, default=1e-4)
    parser.add_argument('--per_device_train_batch_size', type=int, default=1)
    parser.add_argument('--per_device_eval_batch_size', type=int, default=1)
    parser.add_argument('--gradient_accumulation_steps', type=int, default=1)
    parser.add_argument('--eval_steps', type=int, default=10)
    parser.add_argument('--logging_steps', type=int, default=10)
    parser.add_argument('--dataset_text_field', type=str, default='text')
    parser.add_argument('--max_seq_length', type=int, default=120)

    args = parser.parse_args()

    main(args)
