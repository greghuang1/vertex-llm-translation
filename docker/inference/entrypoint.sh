#! /bin/bash
echo "Begin copy ${MODEL_ID} to tmp path..."
mkdir -p /tmp/model && gsutil -m cp -r $MODEL_ID/* /tmp/model
echo "End copy ${MODEL_ID}."
text-generation-launcher --model-id /tmp/model --json-output